﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private int hp = 1;

    private Rigidbody2D rBody;
    private Animator ani;

    private bool IsGround = true;
    private bool CanForward = true;

    void Start()
    {
        rBody = GetComponent<Rigidbody2D>();
        ani = GetComponent<Animator>();
    }

    void Update()
    {
        if (hp <= 0)
        {
            return; 
        }
        float horizontal = Input.GetAxis("Horizontal");
        if (horizontal != 0)
        {
            if (CanForward)
            {
                Vector2 v = rBody.velocity;
                v.x = horizontal * 2;
                rBody.velocity = v;
            }

            GetComponent<SpriteRenderer>().flipX = horizontal > 0 ? true : false;
            ani.SetBool("IsRun", true);
        } else
        {
            ani.SetBool("IsRun", false);
        }

        if (Input.GetKeyDown(KeyCode.K) && IsGround)
        {
            rBody.AddForce(Vector2.up * 270);
            AudioManager.Instance.PlaySound("step_on");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ground")
        {
            IsGround = true;
            ani.SetBool("IsJump", false);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Ground")
        {
            IsGround = false;
            ani.SetBool("IsJump", true);
        }
    }
}
