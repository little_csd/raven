﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float MinX;
    public float MaxX;
    public float MinY;
    public float MaxY;

    public Transform target;

    void Start()
    {
        
    }

    void Update()
    {
        Vector3 v = transform.position;
        v.x = target.position.x;
        v.y = target.position.y + 2.5f;
        if (v.x > MaxX)
        {
            v.x = MaxX;
        }
        else if (v.x < MinX)
        {
            v.x = MinX;
        }
        if (v.y > MaxY)
        {
            v.y = MaxY;
        }
        else if (v.y < MinY)
        {
            v.y = MinY;
        }
        transform.position = v;
    }
}
