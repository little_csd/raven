﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrowFlying : MonoBehaviour
{
    private Rigidbody2D mRigidbody2D;
    private Animator Ani;
    private int threshold = 51;
    private bool BeginFly = false;

    void Start()
    {
        mRigidbody2D = GetComponent<Rigidbody2D>();
        Ani = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        if (!BeginFly) return;
        threshold--;
        if (threshold == 50)
        {
            Debug.Log("add force");
            mRigidbody2D.AddForce(new Vector2(20f, 10f));
            threshold--;
        }
        else if (threshold <= 0 && threshold >= -25)
        {
            mRigidbody2D.AddForce(new Vector2(4f, 0));
            mRigidbody2D.AddForce(new Vector2(0f, 6f));
        }
    }

    public void SetFlyState()
    {
        if (BeginFly) return;
        BeginFly = true;
        Ani.SetTrigger("CanPlay");
        Debug.Log("Change State");
    }
}
