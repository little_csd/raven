﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomHandler : MonoBehaviour
{
    private Text mText;

    private void Start()
    {
        mText = GetComponent<Text>();
    }

    public void OnMouseEnter()
    {
        mText.color = Color.red;
    }

    public void OnMouseExit()
    {
        mText.color = Color.white;
    }
}
