﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterRun : MonoBehaviour
{
    private Animator Ani;
    private Transform mTransform;
    private float Speed = 0.05f;
    private bool IsRun = false;

    void Start()
    {
        Ani = GetComponent<Animator>();
        mTransform = GetComponent<Transform>();
    }

    private void FixedUpdate()
    {
        if (!IsRun) return;
        Vector3 newPosition = new Vector3(mTransform.position.x + Speed, mTransform.position.y);
        mTransform.position = newPosition;
    }

    public void SetRunState()
    {
        Debug.Log(IsRun);

        if (IsRun) return;
        Ani.SetBool("IsRun", true);
        IsRun = true;
    }
}