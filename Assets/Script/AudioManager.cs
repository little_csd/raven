﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;
    private AudioSource mPlayer;

    void Start()
    {
        Instance = this;
        mPlayer = GetComponent<AudioSource>();
    }
    
    // play sound
    public void PlaySound(string name)
    {
        AudioClip clip = Resources.Load<AudioClip>(name);
        mPlayer.PlayOneShot(clip);
    }


    public void StopSound()
    {
        mPlayer.Stop();
    }
}
