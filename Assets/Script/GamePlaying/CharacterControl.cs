﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterControl : MonoBehaviour
{
    private const float INF_F = 36000;
    private const int STATIC = 0;
    private const int JUMP = 1;
    private const int RUN = 2;
    private const int ATTACK = 4;
    private const int BEATTACK = 8;
    private const float MoveSpeed = 8f;

    private const int ATTACK_NULL = 1;
    private const int ATTACK_ZERO = 2;
    private const int ATTACK_ONE = 4;
    private const int ATTACK_TWO = 8;
    private const int ATTACK_THREE = 16;
    private const int ATTACK_FOUR = 32;

    // todo : add skill state here
    private const int SKILL_ZERO = 1;
    private const int SKILL_ONE = 2;

    private const float ATTACK_INTERVAL = 0.6f;
    private const float ATTACK_UPGRADE_INTERVAL = 1.1f;
    private const float BEATTACK_INTERVAL = 0.2f;

    private bool IsGround = false;
    private int AttackState = ATTACK_NULL;
    private float AttackIntervel = 0;
    private float BeAttackInterval = 0;
    private float AttackUpgradeInterval = INF_F;
    private Rigidbody2D mRigidbody2D;
    private Animator Ani;

    public GameObject Attack0;
    public GameObject Attack1;
    public GameObject Attack2;
    public GameObject Attack3;
    public GameObject Attack4;

    private void Start()
    {
        mRigidbody2D = GetComponent<Rigidbody2D>();
        Ani = GetComponent<Animator>();
    }

    void Update()
    {
        CheckHorizontal();
        CheckVertical();
        CheckAttack();
        TimeCalculate();
        
        // for debug
        if (Input.GetKeyUp(KeyCode.Space))
        {
            BeAttack();
        }
    }

    private void CheckVertical()
    {
        if (IsGround && Input.GetKeyDown(KeyCode.K))
        {
            mRigidbody2D.AddForce(new Vector2(0, 700f));
            Ani.SetBool("IsJump", true);
        }
    }

    private void CheckHorizontal()
    {
        if (Ani.GetCurrentAnimatorStateInfo(0).IsName("Attack") || 
            Ani.GetCurrentAnimatorStateInfo(0).IsName("BeAttacked"))
        {
            Vector2 clear = mRigidbody2D.velocity;
            clear.x = 0;
            mRigidbody2D.velocity = clear;
            return;
        }

        float horizontal = Input.GetAxisRaw("Horizontal");
        Vector2 v = mRigidbody2D.velocity;
        v.x = horizontal * MoveSpeed;
        mRigidbody2D.velocity = v;

        if (horizontal != 0)
        {
            bool flip = horizontal > 0 ? false : true;
            bool lastFlip = GetComponent<SpriteRenderer>().flipX;
            if (flip != lastFlip)
            {
                GetComponent<SpriteRenderer>().flipX = flip;
                FlipAttackCollider();
            }
            Ani.SetBool("IsRun", true);
        }
        else
        {
            Ani.SetBool("IsRun", false);
        }
    }

    private void CheckAttack()
    {
        if (!Input.GetKeyDown(KeyCode.J)) return;
        if (BeAttackInterval > 0 || AttackIntervel > 0) return;
        AttackIntervel = ATTACK_INTERVAL;
        Ani.SetTrigger("IsAttack");

        SetAttackCollider(false);

        AttackState = AttackState << 1;
        if (AttackState > ATTACK_FOUR) AttackState = ATTACK_ZERO;
        AttackUpgradeInterval = ATTACK_UPGRADE_INTERVAL;
        SetAniAttackState();

        SetAttackCollider(true);
    }

    private void SetAniAttackState()
    {
        if (AttackState == ATTACK_ZERO) Ani.SetTrigger("Attack0");
        else if (AttackState == ATTACK_ONE) Ani.SetTrigger("Attack1");
        else if (AttackState == ATTACK_TWO) Ani.SetTrigger("Attack2");
        else if (AttackState == ATTACK_THREE) Ani.SetTrigger("Attack3");
        else if (AttackState == ATTACK_FOUR) Ani.SetTrigger("Attack4");
    }

    private void SetAttackCollider(bool State)
    {
        if (AttackState == ATTACK_ZERO)
        {
            Attack0.SetActive(State);
        }
        else if (AttackState == ATTACK_ONE)
        {
            Attack1.SetActive(State);
        }
        else if (AttackState == ATTACK_TWO)
        {
            Attack2.SetActive(State);
        }
        else if (AttackState == ATTACK_THREE)
        {
            Attack3.SetActive(State);
        }
        else if (AttackState == ATTACK_FOUR)
        {
            Attack4.SetActive(State);
        }
    }
    
    private void FlipAttackCollider()
    {
        Attack0.transform.Rotate(new Vector3(0, 180, 0));
        Attack1.transform.Rotate(new Vector3(0, 180, 0));
        Attack2.transform.Rotate(new Vector3(0, 180, 0));
        Attack3.transform.Rotate(new Vector3(0, 180, 0));
        Attack4.transform.Rotate(new Vector3(0, 180, 0));
    }

    private void TimeCalculate()
    {
        if (AttackIntervel > 0)
        {
            AttackIntervel -= Time.deltaTime;
            if (AttackIntervel < 0) AttackIntervel = 0;
        }
        if (BeAttackInterval > 0)
        {
            BeAttackInterval -= Time.deltaTime;
            if (BeAttackInterval < 0) BeAttackInterval = 0;
            Ani.SetBool("IsBeAttack", false);
        }
        if (AttackUpgradeInterval > 0)
        {
            AttackUpgradeInterval -= Time.deltaTime;
            if (AttackUpgradeInterval < 0)
            {
                SetAttackCollider(false);
                AttackUpgradeInterval = INF_F;
                AttackState = ATTACK_NULL;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Ground"))
        {
            IsGround = true;
            Ani.SetBool("IsJump", false);
        }
        /*else if (collision.CompareTag("Attack"))
        {
            // todo : change state here

            BeAttackInterval = BEATTACK_INTERVAL;
        }*/
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Ground"))
        {
            IsGround = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Ground"))
        {
            IsGround = false;
        }
    }

    // for debug
    public void BeAttack()
    {
        Debug.Log("I was attacked!");
        Ani.SetBool("IsBeAttack", true);
        BeAttackInterval = BEATTACK_INTERVAL;
    }
}
